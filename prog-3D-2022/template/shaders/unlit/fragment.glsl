#version 330 core

in vec3 o_positionWorld;
in vec3 o_normalWorld;
in vec2 o_uv0;
in vec3 o_lightPos;
in vec3 o_tangent;

out vec4 FragColor;
uniform vec3 lightColor;
uniform vec4 color;
uniform sampler2D colorTexture;

struct MaterialComponents {
    vec3 ambient ;
    vec3 diffuse ;
    vec3 specular ;
    float shininess ;
};

uniform MaterialComponents material;

void main() {
    //FragColor = color;
    FragColor = texture(colorTexture, o_uv0) * color;
    // DEBUG: position
    //FragColor = vec4(o_positionWorld, 1.0);
    // DEBUG: normal
    //FragColor = vec4(0.5 * o_normalWorld + vec3(0.5) , 1.0);
    // DEBUG: uv0
    // FragColor = vec4(o_uv0, 1.0);
}
