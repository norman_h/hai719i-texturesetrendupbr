#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec2 uv0;
layout(location = 4) in vec3 ambient;
layout(location = 5) in vec3 diffuse;
layout(location = 6) in vec3 specular;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 lightPos;

//uniform mat4 mvp;
//uniform mat4 modelView;
//uniform mat4 normalMatrix;

out vec3 o_positionWorld;
out vec3 o_normalWorld;
out vec3 o_tangent;
out vec2 o_uv0;
out vec3 o_lightPos;

out vec3 o_ambient;
out vec3 o_diffuse;
out vec3 o_specular;

void main() {
  mat3 normalMatrix = mat3(transpose(inverse(model)));
  o_uv0 = uv0;
  vec4 positionWorld = model * vec4(position, 1.0);
  o_positionWorld = positionWorld.xyz;
  o_normalWorld = normalMatrix * normal;
  gl_Position = projection * view * positionWorld;
}
